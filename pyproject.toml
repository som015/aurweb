[tool.black]
line-length = 88

[tool.isort]
profile = "black"
combine_as_imports = true


# Poetry build configuration for the aurweb project.
#
# Dependencies:
#   * python >= 3.9
#   * pip
#       * poetry
#       * poetry-dynamic-versioning
#
[tool.poetry]
name = "aurweb"
version = "v6.1.9"
license = "GPL-2.0-only"
description = "Source code for the Arch User Repository's website"
homepage = "https://aur.archlinux.org"
repository = "https://gitlab.archlinux.org/archlinux/aurweb"
documentation = "https://gitlab.archlinux.org/archlinux/aurweb/-/blob/master/README.md"
keywords = ["aurweb", "aur", "Arch", "Linux"]
authors = [
    "Lucas Fleischer <lfleischer@archlinux.org>",
    "Eli Schwartz <eschwartz@archlinux.org>",
    "Kevin Morris <kevr@0cost.org>"
]
maintainers = [
    "Eli Schwartz <eschwartz@archlinux.org>"
]
packages = [
    { include = "aurweb" }
]

[tool.poetry-dynamic-versioning]
enable = true
vcs = "git"

[build-system]
requires = ["poetry>=1.1.8", "poetry-dynamic-versioning"]
build-backend = "poetry.masonry.api"

[tool.poetry.urls]
"Repository" = "https://gitlab.archlinux.org/archlinux/aurweb"
"Bug Tracker" = "https://gitlab.archlinux.org/archlinux/aurweb/-/issues"
"Development Mailing List" = "https://lists.archlinux.org/listinfo/aur-dev"
"General Mailing List" = "https://lists.archlinux.org/listinfo/aur-general"
"Request Mailing List" = "https://lists.archlinux.org/listinfo/aur-requests"

[tool.poetry.dependencies]
python = ">=3.9,<3.11"

# poetry-dynamic-versioning is used to produce tool.poetry.version
# based on git tags.

# General
aiofiles = "^22.0.0"
asgiref = "^3.4.1"
bcrypt = "^4.0.0"
bleach = "^5.0.0"
email-validator = "^1.3.0"
fakeredis = "^2.0.0"
feedgen = "^0.9.0"
httpx = "^0.23.1"
itsdangerous = "^2.0.1"
lxml = "^4.6.3"
orjson = "^3.8.2"
protobuf = "^4.21.9"
pygit2 = "^1.11.1"
python-multipart = "^0.0.5"
redis = "^4.3.5"
requests = "^2.28.1"
paginate = "^0.5.6"

# SQL
alembic = "^1.7.4"
mysqlclient = "^2.0.3"
Authlib = "^1.0.0"
Jinja2 = "^3.0.2"
Markdown = "^3.3.6"
Werkzeug = "^2.0.2"
SQLAlchemy = "^1.4.26"

# ASGI
uvicorn = "^0.20.0"
gunicorn = "^20.1.0"
Hypercorn = "^0.14.0"
prometheus-fastapi-instrumentator = "^5.7.1"
pytest-xdist = "^3.0.2"
filelock = "^3.3.2"
posix-ipc = "^1.0.5"
pyalpm = "^0.10.6"
fastapi = "^0.87.0"
srcinfo = "^0.0.8"

[tool.poetry.dev-dependencies]
coverage = "^6.0.2"
pytest = "^7.2.0"
pytest-asyncio = "^0.20.2"
pytest-cov = "^4.0.0"
pytest-tap = "^3.2"

[tool.poetry.scripts]
aurweb-git-auth = "aurweb.git.auth:main"
aurweb-git-serve = "aurweb.git.serve:main"
aurweb-git-update = "aurweb.git.update:main"
aurweb-aurblup = "aurweb.scripts.aurblup:main"
aurweb-mkpkglists = "aurweb.scripts.mkpkglists:main"
aurweb-notify = "aurweb.scripts.notify:main"
aurweb-pkgmaint = "aurweb.scripts.pkgmaint:main"
aurweb-popupdate = "aurweb.scripts.popupdate:main"
aurweb-rendercomment = "aurweb.scripts.rendercomment:main"
aurweb-tuvotereminder = "aurweb.scripts.tuvotereminder:main"
aurweb-usermaint = "aurweb.scripts.usermaint:main"
aurweb-config = "aurweb.scripts.config:main"
aurweb-adduser = "aurweb.scripts.adduser:main"
aurweb-git-archive = "aurweb.scripts.git_archive:main"
